# SPDX-License-Identifier: GPL-2.0-or-later OR AGPL-3.0-or-later OR CERN-OHL-S-2.0+
import os, subprocess, site
import pathlib
from os.path import relpath, abspath
from itertools import chain
from pathlib import Path
from textwrap import dedent

from doit import get_var
from doit.action import BaseAction, CmdAction
from doit.tools import check_timestamp_unchanged, create_folder

import pdkmaster, c4m, c4m.flexcell


### Config


DOIT_CONFIG = {
    "default_tasks": [
        "install", "coriolis", "klayout", "spice_models", "spice", "gds", "rtl",
        "liberty",
    ],
}

### support functions


def get_var_env(name, default=None):
    """Uses get_var to get a command line variable, also checks
    environment variables for default value

    If os.environ[name.upper()] exists that value will override the
    default value given.
    """
    try:
        default = os.environ[name.upper()]
    except:
        # Keep the specified default
        pass
    return get_var(name, default=default)


class AVTScriptAction(BaseAction):
    def __init__(self, avt_script, tmp=None):
        if tmp is None:
            tmp = tmp_dir
        self.script = avt_script
        self.tmp = tmp

        self.out = None
        self.err = None
        self.result = None
        self.values = {}

    def execute(self, out=None, err=None):
        # Create new action on every new call so we can always write
        # the script to the stdin of the subprocess.
        if avt_shell is None:
            action = CmdAction(("echo", "disabled because lack of avt_shell"))
        else:
            pr, pw = os.pipe()
            fpw = os.fdopen(pw, "w")
            fpw.write(self.script)
            fpw.close()

            action = CmdAction(avt_shell, stdin=pr, cwd=self.tmp)

        r = action.execute(out=out, err=err)
        self.values = action.values
        self.result = action.result
        self.out = action.out
        self.err = action.err
        return r


### globals


top_dir = Path(__file__).parent
tmp_dir = top_dir.joinpath("tmp")
scripts_dir = top_dir.joinpath("scripts")
open_pdk_dir = top_dir.joinpath("open_pdk", "C4M.Sky130")
open_pdk_tech_dir = open_pdk_dir.joinpath("libs.tech")
open_pdk_ref_dir = open_pdk_dir.joinpath("libs.ref")
coriolis_dir = open_pdk_tech_dir.joinpath("coriolis", "techno")
corio_nda_dir = coriolis_dir.joinpath("etc", "coriolis2", "NDA")

# variables
pdkmaster_pip = get_var_env("pdkmaster_pip", default="pip3")
coriolis_python = get_var_env("coriolis_python", default="python2")
avertec_top = get_var_env("avertec_top")
avt_shell = get_var_env(
    "avt_shell", default=(
        f"{avertec_top}/bin/avt_shell" if avertec_top is not None else None
    ),
)
sky130_pdk = get_var_env("sky130_pdk")
if sky130_pdk is None:
    raise EnvironmentError(
        "sky130_pdk variable or SKY130_PDK environment variable not given"
    )
os.environ["SKY130_PDK"] = sky130_pdk
sky130_pdk_dir = pathlib.Path(sky130_pdk)
sky130_pdk_spice_dir = sky130_pdk_dir.joinpath(
    "libs.ref", "sky130_fd_pr", "spice"
)

# Module dirs are derived with a task as they are dependent on pdkmasteR_python
pdkmaster_inst_dir = Path(pdkmaster.__file__).parent
# Don't use local module for c4m
c4m_inst_dir = Path(site.getsitepackages()[0]).joinpath("c4m")
c4m_local_dir = top_dir.joinpath("c4m")
sky130_inst_dir = c4m_inst_dir.joinpath("pdk", "sky130")
flexcell_inst_dir = Path(c4m.flexcell.__file__).parent

c4m_py_files = tuple(c4m_local_dir.rglob("*.py"))

# Hard codes values
# These are hard coded to avoid having to bring in sky130 each time doit is
# called
sky130_cells = {
    "FlexLib": (
        "a2_x2", "a3_x2", "a4_x2", "ao22_x2", "buf_x2", "buf_x4", "buf_x8", "dio_x0",
        "fill_x0", "inv_x1", "inv_x2", "inv_x4", "inv_x8", "mx2_x2", "mx3_x2", "na2_x1",
        "na2_x4", "na3_x1", "na4_x1", "no2_x1", "no3_x1", "no4_x1", "nsnrlatch_x1",
        "nxr2_x1", "o2_x2", "o3_x2", "o4_x2", "oa22_x2", "one_x0", "sff1_x4", "sff1r_x4",
        "tie_x0", "xr2_x1", "zero_x0",
    ),
}



### main tasks


#
# install
def task_install():
    """Install the python module"""

    return {
        "title": lambda _: "Installing python module",
        "file_dep": c4m_py_files,
        "targets": (sky130_inst_dir,),
        "actions": (
            f"{pdkmaster_pip} install {top_dir}",
        ),
    }


#
# coriolis
def task_coriolis():
    """Generate coriolis support files"""

    corio_node130_dir = corio_nda_dir.joinpath("node130")
    corio_sky130_dir = corio_node130_dir.joinpath("sky130")

    corio_nda_init_file = corio_nda_dir.joinpath("__init__.py")
    corio_node130_init_file = corio_node130_dir.joinpath("__init__.py")
    corio_sky130_init_file = corio_sky130_dir.joinpath("__init__.py")
    corio_sky130_techno_file = corio_sky130_dir.joinpath("techno.py")
    corio_sky130_lib_files = tuple(
        corio_sky130_dir.joinpath(f"{lib}.py") for lib in sky130_cells.keys()
    ) + (corio_sky130_dir.joinpath("FlexLib_fix.py"),)

    def gen_init():
        from c4m.pdk import sky130

        with corio_sky130_init_file.open("w") as f:
            f.write("from .techno import *\n")
            for lib in sky130.__libs__:
                f.write(f"from .{lib.name} import setup as {lib.name}_setup\n")

            f.write(
                "\n__lib_setups__ = [{}]".format(
                    ",".join(f"{lib.name}.setup" for lib in sky130.__libs__)
                )
            )

    def gen_coriolis():
        from pdkmaster.io.export import coriolis
        from c4m.pdk import sky130

        gen = coriolis.CoriolisGenerator(sky130.tech)

        with corio_sky130_techno_file.open("w") as f:
            f.write(dedent("""
                # Autogenerated file
                # SPDX-License-Identifier: GPL-2.0-or-later OR AGPL-3.0-or-later OR CERN-OHL-S-2.0+
            """))
            f.write(gen())

        for lib in sky130.__libs__:
            with corio_sky130_dir.joinpath(f"{lib.name}.py").open("w") as f:
                f.write(gen(lib))

    def gen_flexlib_fix():
        with corio_sky130_dir.joinpath("FlexLib_fix.py").open("w") as f:
            f.write(dedent("""
                from Hurricane import Net

                def fix(lib):
                    for cell in lib.getCells():
                        for net in cell.getNets():
                            if net.getName() == 'vdd':
                                net.setType( Net.Type.POWER )
                                net.setDirection( Net.Direction.IN )
                            elif net.getName() == 'vss':
                                net.setType( Net.Type.GROUND )
                                net.setDirection( Net.Direction.IN )
                            elif net.getName() == 'ck':
                                net.setType( Net.Type.CLOCK )
                                net.setDirection( Net.Direction.IN )
                            elif net.getName() == 'nq' or net.getName() == "q":
                                net.setDirection( Net.Direction.OUT )
                            else:
                                net.setDirection( Net.Direction.IN )
            """[1:]))

    return {
        "title": lambda _: "Creating coriolis files",
        "file_dep": c4m_py_files,
        "uptodate": (
            check_timestamp_unchanged(str(pdkmaster_inst_dir)),
            check_timestamp_unchanged(str(flexcell_inst_dir)),
        ),
        "targets": (
            corio_nda_init_file, corio_node130_init_file, corio_sky130_init_file,
            corio_sky130_techno_file, *corio_sky130_lib_files,
        ),
        "actions": (
            (create_folder, (corio_sky130_dir,)),
            corio_nda_init_file.touch, corio_node130_init_file.touch,
            gen_init, gen_coriolis, gen_flexlib_fix,
        ),
        "clean": True,
    }


#
# klayout
def task_klayout():
    """Generate klayout files"""

    klayout_dir = open_pdk_tech_dir.joinpath("klayout")
    klayout_tech_dir = klayout_dir.joinpath("tech", "C4M.Sky130")
    klayout_drc_dir = klayout_tech_dir.joinpath("drc")
    klayout_lvs_dir = klayout_tech_dir.joinpath("lvs")
    klayout_share_dir = klayout_dir.joinpath("share")
    klayout_bin_dir = klayout_dir.joinpath("bin")

    klayout_lyt_file = klayout_tech_dir.joinpath("C4M.Sky130.lyt")
    klayout_drc_lydrc_file = klayout_drc_dir.joinpath("DRC.lydrc")
    klayout_extract_lylvs_file = klayout_lvs_dir.joinpath("Extract.lylvs")
    klayout_drc_file = klayout_share_dir.joinpath("Sky130.drc")
    klayout_drc_script = klayout_bin_dir.joinpath("drc_Sky130")
    klayout_extract_file = klayout_share_dir.joinpath("Sky130_extract.lvs")
    klayout_extract_script = klayout_bin_dir.joinpath("extract_Sky130")
    klayout_lvs_file = klayout_share_dir.joinpath("Sky130.lvs")
    klayout_lvs_script = klayout_bin_dir.joinpath("lvs_Sky130")

    def gen_klayout():
        from pdkmaster.io.export.klayout import Generator
        from c4m.pdk import sky130
        from xml.etree.ElementTree import ElementTree

        expo = Generator(sky130.tech, export_name=f"C4M.{sky130.tech.name}")()

        # DRC
        with klayout_drc_file.open("w") as f:
            f.write(expo["drc"])
        with klayout_drc_script.open("w") as f:
            relfile = relpath(klayout_drc_file, klayout_bin_dir)
            f.write(dedent(f"""
                #!/bin/sh
                d=`dirname $0`
                deck=`realpath $d/{relfile}`

                if [ $# -ne 2 ]
                then
                    echo "Usage `basename $0` input report"
                    exit 20
                fi

                export SOURCE_FILE=$1 REPORT_FILE=$2
                klayout -b -r ${{deck}}
            """[1:]))
        klayout_drc_script.chmod(0o755)

        # Extract
        with klayout_extract_file.open("w") as f:
            f.write(expo["extract"])
        with klayout_extract_script.open("w") as f:
            relfile = relpath(klayout_extract_file, klayout_bin_dir)
            f.write(dedent(f"""
                #!/bin/sh
                d=`dirname $0`
                deck=`realpath $d/{relfile}`

                if [ $# -ne 2 ]
                then
                    echo "Usage `basename $0` input spice_out"
                    exit 20
                fi

                export SOURCE_FILE=$1 SPICE_FILE=$2
                klayout -b -r ${{deck}}
            """[1:]))
        klayout_extract_script.chmod(0o755)

        # LVS
        with klayout_lvs_file.open("w") as f:
            f.write(expo["lvs"])
        with klayout_lvs_script.open("w") as f:
            relfile = relpath(klayout_lvs_file, klayout_bin_dir)
            f.write(dedent(f"""
                #!/bin/sh
                d=`dirname $0`
                deck=`realpath $d/{relfile}`

                if [ $# -ne 3 ]
                then
                    echo "Usage `basename $0` gds spice report"
                    exit 20
                fi

                export SOURCE_FILE=`realpath $1` SPICE_FILE=`realpath $2` REPORT_FILE=$3
                klayout -b -r ${{deck}}
            """[1:]))

        # klayout technology
        et = ElementTree(expo["ly_drc"])
        et.write(klayout_drc_lydrc_file, encoding="utf-8", xml_declaration=True)
        et = ElementTree(expo["ly_extract"])
        et.write(klayout_extract_lylvs_file, encoding="utf-8", xml_declaration=True)
        et = ElementTree(expo["ly_tech"])
        et.write(klayout_lyt_file, encoding="utf-8", xml_declaration=True)

    return {
        "title": lambda _: "Creating klayout files",
        "file_dep": c4m_py_files,
        "uptodate": (
            check_timestamp_unchanged(str(pdkmaster_inst_dir)),
        ),
        "task_dep": (
        ),
        "targets": (
            klayout_lyt_file, klayout_drc_lydrc_file, klayout_extract_lylvs_file,
            klayout_drc_file, klayout_drc_script, klayout_extract_file,
            klayout_extract_script, klayout_lvs_file, klayout_lvs_script,
        ),
        "actions": (
            (create_folder, (klayout_share_dir,)),
            (create_folder, (klayout_bin_dir,)),
            (create_folder, (klayout_drc_dir,)),
            (create_folder, (klayout_lvs_dir,)),
            gen_klayout,
        ),
        "clean": True,
    }


#
# spice_models
open_pdk_spice_dir = open_pdk_tech_dir.joinpath("ngspice")
spice_model_files = {
    corner: {
        "deps": (
            sky130_pdk_spice_dir.joinpath(
                f"sky130_fd_pr__nfet_01v8__{corner}.pm3.spice",
            ),
            sky130_pdk_spice_dir.joinpath(
                f"sky130_fd_pr__pfet_01v8__{corner}.pm3.spice",
            ),
        ),
        "targets": (
            open_pdk_spice_dir.joinpath(f"C4M.Sky130_{corner}_model.spice"),
        ),
    }
    for corner in ("tt", "ff", "ss", "fs", "sf")
}
def task_spice_models():
    """Convert Sky130 spice model files to own format"""

    def spice_model_title(task):
        corner = task.name[13:]
        return f"Converting spice model file for corner {corner}"

    def convert_spice(corner):
        import re

        files = spice_model_files[corner]
        with files["targets"][0].open("w") as f_target:
            for dep in files["deps"]:
                with dep.open("r") as f_dep:
                    # We start with ignoring the lines
                    output = False
                    for line in f_dep:
                        # Extract only the .model decks
                        if line.startswith(".model"):
                            output = True
                        if line.startswith(".ends"):
                            output = False
                        if output:
                            # Remove l/w parameter dependency
                            # These are the {} sections and take just the first
                            # number
                            s = re.search("{(-?\d+(\.\d*)?(e(-|\+)?\d+)?).*}", line)
                            if s:
                                line = line[:s.start()] + s.groups()[0] + line[s.end():]
                            f_target.write(line)

    for corner in spice_model_files.keys():
        files = spice_model_files[corner]
        yield {
            "title": spice_model_title,
            "name": corner,
            "doc": f"Converting spice model file for corner {corner}",
            "file_dep": files["deps"],
            "targets": files["targets"],
            "actions": (
                (create_folder, (open_pdk_spice_dir,)),
                (convert_spice, (corner,)),
            )
        }


#
# spice
def task_spice():
    """Generate SPICE files"""

    spice_dirs = tuple(
        open_pdk_ref_dir.joinpath(lib, "spice") for lib in sky130_cells.keys()
    )
    spice_files = tuple(chain(
        *(
            (
                open_pdk_ref_dir.joinpath(lib, "spice", f"{cell}.spi")
                for cell in cells
            ) for lib, cells in sky130_cells.items()
        ),
        (
            open_pdk_ref_dir.joinpath(lib, "spice", f"{lib}.spi")
            for lib in sky130_cells.keys()
        ),
    ))

    def gen_spice():
        from c4m.pdk import sky130

        for lib in sky130.__libs__:
            lib_spice_dir = open_pdk_ref_dir.joinpath(lib.name, "spice")
            with lib_spice_dir.joinpath(f"{lib.name}.spi").open("w") as f_lib:
                f_lib.write(f"* {lib.name}\n")
                for cell in lib.cells:
                    pyspicesubckt = sky130.pyspicefab.new_pyspicesubcircuit(
                        circuit=cell.circuit
                    )
                    s = f"* {cell.name}\n" + str(pyspicesubckt)
                    f_lib.write("\n" + s)
                    with lib_spice_dir.joinpath(f"{cell.name}.spi").open("w") as f_cell:
                        f_cell.write(s)

    return {
        "title": lambda _: "Creating spice files",
        "file_dep": c4m_py_files,
        "uptodate": (
            check_timestamp_unchanged(str(pdkmaster_inst_dir)),
            check_timestamp_unchanged(str(flexcell_inst_dir)),
        ),
        "targets": spice_files,
        "actions": (
            *(
                (create_folder, (dir_,)) for dir_ in spice_dirs
            ),
            gen_spice,
        ),
        "clean": True,
    }


#
# gds
def task_gds():
    """Generate GDSII files"""

    gds_dirs = tuple(
        open_pdk_ref_dir.joinpath(lib, "gds") for lib in sky130_cells.keys()
    )
    gds_files = tuple(chain(*(
        (
            open_pdk_ref_dir.joinpath(lib, "gds", f"{cell}.gds")
            for cell in cells
        ) for lib, cells in sky130_cells.items()
    )))

    def gen_gds():
        coriolis_script = dedent(f"""
            import os, CRL
            from helpers import setNdaTopDir

            setNdaTopDir("{str(coriolis_dir)}")

            from NDA.node130 import sky130

            sky130.setup()

            for setup in sky130.__lib_setups__:
                lib = setup()
                gdsdir = "{str(open_pdk_ref_dir)}/{{}}/gds".format(lib.getName())
                olddir = os.curdir
                os.chdir(gdsdir)
                for cell in lib.getCells():
                    CRL.Gds.save(cell)
                os.chdir(olddir)
        """[1:])
        r = subprocess.run(
            (coriolis_python, "-c", coriolis_script), stdout=subprocess.PIPE,
        )
        return r.returncode == 0

    return {
        "title": lambda _: "Creating gds files",
        "file_dep": c4m_py_files,
        "task_dep": ("coriolis",),
        "uptodate": (
            check_timestamp_unchanged(str(pdkmaster_inst_dir)),
            check_timestamp_unchanged(str(flexcell_inst_dir)),
        ),
        "task_dep": ("coriolis",),
        "targets": gds_files,
        "actions": (
            *(
                (create_folder, (dir_,)) for dir_ in gds_dirs
            ),
            gen_gds,
        ),
        "clean": True,
    }


#
# VHDL/Verilog
def task_rtl():
    """Generate VHDL/verilog files"""

    def rtl_targets(lang):
        suffix = {
            "vhdl": "vhdl",
            "verilog": "v",
        }[lang]

        return tuple(chain(*(
            (
                open_pdk_ref_dir.joinpath(lib, lang, f"{cell}.{suffix}")
                for cell in cells
            ) for lib, cells in sky130_cells.items()
        )))

    def rtl_dirs(lang):
        return (tmp_dir, *(
            open_pdk_ref_dir.joinpath(lib, lang)
            for lib in sky130_cells.keys()
        ))

    def rtl_script(lang):
        avt_shell_script = dedent(f"""
            avt_config simToolModel hspice
            avt_LoadFile "{sky130_pdk_spice_dir.joinpath("sky130_fd_pr__nfet_01v8__tt.pm3.spice")}" spice
            avt_LoadFile "{sky130_pdk_spice_dir.joinpath("sky130_fd_pr__pfet_01v8__tt.pm3.spice")}" spice
            avt_config avtVddName "vdd"
            avt_config avtVssName "vss"
            avt_config yagNoSupply "yes"
        """[1:])

        if lang == "verilog":
            avt_shell_script += dedent("""
                avt_config avtOutputBehaviorFormat "vlg"
                set map {spice verilog .spi .v}
                set suffix v
                set comment "//"
            """[1:])
        elif lang == "vhdl":
            avt_shell_script += dedent("""
                avt_config avtOutputBehaviorFormat "vhd"
                set map {spice vhdl .spi .vhdl}
                set suffix vhd
                set comment "--"
            """[1:])
        else:
            raise NotImplementedError(f"rtl lang {lang}")

        avt_shell_script += "foreach spice_file {\n"
        avt_shell_script += "\n".join(chain(*(
            (
                f'    "{str(open_pdk_ref_dir.joinpath(lib, "spice", f"{cell}.spi"))}"'
                for cell in cells
            ) for lib, cells in sky130_cells.items()
        ))) + "\n"
        avt_shell_script += dedent("""
            } {
                avt_LoadFile $spice_file spice
                set rtl_file [string map $map $spice_file]
                set cell [string map {.spi ""} [file tail $spice_file]]
                if {[string match "sff1*" $cell]} {
                    inf_SetFigureName $cell
                    inf_MarkSignal sff_m "FLIPFLOP+MASTER"
                    inf_MarkSignal sff_s SLAVE
                }
                set out_file "$cell.$suffix"
                yagle $cell
                if [file exists $out_file] {
                    file copy -force $out_file $rtl_file
                } else {
                    set f [open $rtl_file w]
                    puts $f "$comment no model for $cell"
                }
            }
        """[1:])

        return avt_shell_script

    def rtl_title(task):
        return (
            f"Creating {task.name[4:]} files" if avt_shell is not None
            else f"missing avt_shell; no {task.name[4:]} files created"
        )

    for lang in ("vhdl", "verilog"):
        yield {
            "name": lang,
            "doc": {
                "vhdl": "Generate VHDL files",
                "verilog": "Generate Verilog files",
            }[lang],
            "title": rtl_title,
            "file_dep": c4m_py_files,
            "uptodate": (
                check_timestamp_unchanged(str(pdkmaster_inst_dir)),
                check_timestamp_unchanged(str(flexcell_inst_dir)),
            ),
            "task_dep": ("spice",),
            "targets": rtl_targets(lang),
            "actions": (
                *(
                    (create_folder, (dir_,)) for dir_ in rtl_dirs(lang)
                ),
                AVTScriptAction(rtl_script(lang)),
            )
        }


#
# liberty
def task_liberty():
    """Generate liberty files"""

    liberty_libs = ("FlexLib",)
    liberty_spice_corners = {
        "nom": "tt", "fast": "ff", "slow": "ss",
    }

    def liberty_target(lib, corner):
        return open_pdk_ref_dir.joinpath(lib, "liberty", f"{lib}_{corner}.lib")

    def liberty_dir(lib):
        return open_pdk_ref_dir.joinpath(lib, "liberty")

    def liberty_title(task):
        lib, corner = task.name[8:].split("_")
        return (
            f"Creating liberty files for library {lib}, corner {corner}" if avt_shell is not None
            else "missing avt_shell; no liberty files created for library {lib}, corner {corner}"
        )

    def liberty_script(lib, corner):
        assert lib in ("FlexLib",), "Unsupported lib"

        avt_script = dedent("""
            avt_config simToolModel hspice
            avt_config avtVddName "vdd"
            avt_config avtVssName "vss"
            avt_config tasBefig yes
            avt_config tmaDriveCapaout yes
            avt_config avtPowerCalculation yes
            avt_config simSlope 20e-12
        """[1:])

        if corner == "nom":
            avt_script += dedent(f"""
                avt_config simPowerSupply 1.8
                avt_config simTemperature 25
            """[1:])
        elif corner == "fast":
            avt_script += dedent(f"""
                avt_config simPowerSupply 1.98
                avt_config simTemperature -20
            """[1:])
        elif corner == "slow":
            avt_script += dedent(f"""
                avt_config simPowerSupply 1.62
                avt_config simTemperature 85
            """[1:])
        else:
            raise NotImplementedError(f"corner {corner}")

        spice_file = open_pdk_ref_dir.joinpath(lib, "spice", f"{lib}.spi")
        spice_corner = liberty_spice_corners[corner]
        avt_script += dedent(f"""
            avt_LoadFile "{open_pdk_spice_dir}/C4M.Sky130_{spice_corner}_model.spice" spice
            avt_config tmaLibraryName {lib}_{corner}
            avt_LoadFile {spice_file} spice
            
            foreach cell {{
        """[1:])
        avt_script += "".join(f"    {cell}\n" for cell in sky130_cells[lib])
        verilog_dir = open_pdk_ref_dir.joinpath(lib, "verilog")
        liberty_file = open_pdk_ref_dir.joinpath(lib, "liberty", f"{lib}_{corner}.lib")
        avt_script += dedent(f"""
            }} {{
                set verilogfile {verilog_dir}/$cell.v

                if {{[string match "sff1*" $cell]}} {{
                    # TODO: make these settings configurable
                    set beh_fig NULL
                    inf_SetFigureName $cell
                    inf_MarkSignal sff_m "FLIPFLOP+MASTER"
                    inf_MarkSignal sff_s SLAVE
                    create_clock -period 3000 ck
                }} elseif {{[string match "*latch*" $cell]}} {{
                    set beh_fig NULL
                }} else {{
                    set beh_fig [avt_LoadBehavior $verilogfile verilog]
                }}
                set tma_fig [tma_abstract [hitas $cell] $beh_fig]

                lappend tma_list $tma_fig
                lappend beh_list $beh_fig
            }}

            lib_drivefile $tma_list $beh_list "{liberty_file}" max
        """[1:])

        return avt_script

    for lib in liberty_libs:
        for corner in ("nom", "fast", "slow"):
            spice_corner = liberty_spice_corners[corner]
            tmp = tmp_dir.joinpath(f"{lib}_{corner}")
            yield {
                "name": f"{lib}_{corner}",
                "doc": f"Generate liberty file for {lib}; {corner} corner",
                "title": liberty_title,
                "file_dep": c4m_py_files,
                "uptodate": (
                    check_timestamp_unchanged(str(pdkmaster_inst_dir)),
                    check_timestamp_unchanged(str(flexcell_inst_dir)),
                ),
                "task_dep": ("spice", "rtl:verilog", f"spice_models:{spice_corner}"),
                "targets": (liberty_target(lib, corner),),
                "actions": (
                    (create_folder, (liberty_dir(lib),)),
                    (create_folder, (tmp,)),
                    AVTScriptAction(liberty_script(lib, corner), tmp=tmp),
                ),
            }
