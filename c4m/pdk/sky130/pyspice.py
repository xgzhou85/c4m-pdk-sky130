# SPDX-License-Identifier: GPL-2.0-or-later OR AGPL-3.0-or-later OR CERN-OHL-S-2.0+
import os
from os.path import exists
from warnings import warn

from pdkmaster import _util
from pdkmaster.io.export import pyspice

__all__ = ["pyspicefab", "pyspice_factory"]


class _PySpiceFactory(pyspice.PySpiceFactory):
    def new_pyspicecircuit(self, **kwargs):
        corner = kwargs["corner"]
        if isinstance(corner, str) or (not _util.is_iterable(corner)):
            corner = (corner,)
        return super().new_pyspicecircuit(**kwargs)


pyspicefab = pyspice_factory = None
try:
    sky130_pdk = os.environ["SKY130_PDK"]
except:
    warn("SKY130_PDK not found, no pypspice factory available for Sky130 PDK")
else:
    libfile = f"{sky130_pdk}/libs.tech/ngspice/sky130.lib.spice"
    if exists(libfile):
        pyspicefab = pyspice_factory = _PySpiceFactory(
            libfile=libfile,
            corners=("tt", "sf", "ff", "ss", "fs"),
            conflicts={
                "tt": ("sf", "ff", "ss", "fs"),
                "sf": ("tt", "ff", "ss", "fs"),
                "ff": ("tt", "sf", "ss", "fs"),
                "ss": ("tt", "sf", "ff", "fs"),
                "fs": ("tt", "sf", "ff", "ss"),
            },
        )
    else:
        warn(f"{libfile} not found\nno pypspice factory available for Sky130 PDK")
