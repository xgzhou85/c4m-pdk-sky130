# SPDX-License-Identifier: GPL-2.0-or-later OR AGPL-3.0-or-later OR CERN-OHL-S-2.0+
from pdkmaster.technology import (
    property_ as prp, mask as msk, primitive as prm, technology_ as tch
)
from pdkmaster.design import layout as lay, circuit as ckt

__all__ = [
    "tech", "technology", "layoutfab", "layout_factory",
    "cktfab", "circuit_factory", "plotter",
]


# TODO: Add copper back-end information
class _Sky130(tch.Technology):
    name = "Sky130"
    grid = 0.005
    substrate_type = "p"

    def _init(self):
        prims = self._primitives

        # TODO: angle design rules

        # For li/metal layer we fix datatype to 20
        prim_layers = {
            "nwm": (64, 20), # NWell
            # "dnm": (64, 18),
            "difftap": (65, 20), # We will generate diff and tap from difftap
            "nsdm": (93, 44), # N+ implant
            "psdm": (94, 20), # P+ implant
            "lvtn": (125, 44), # low-Vt adjust
            "hvtp": (78, 44), # pmos high-Vt adjust
            "hvi": (75, 20), # Thick oxide
            "poly": (66, 20),
            "licon": (66, 44),
            "li": (67, 20), # We fix it to datatype 20
            "mcon": (67, 44),
            "m1": (68, 20),
            "via": (68, 44),
            "m2": (68, 20),
            "via2": (68, 44),
            "m3": (68, 20),
            "via3": (68, 44),
            "m4": (68, 20),
            "via4": (68, 44),
            "m5": (68, 20),
            "via5": (68, 44),
            "pad": (76, 20),
            # Unhandled layers
            # "pwbm": (19, 44),
            # "pwde": (124, 20),
            # "natfet": (124, 21),
            # "hvtr": (18,20),
            # "ldntm": (11, 44),
            # "tunm": (80,20),
            # "hvntm": (125, 20),
            # "rpm": (86, 20),
            # "urpm": (79, 20),
            # "npc": (95, 20),
            # "nsm": (61, 20),
            # "capm": (89, 44),
            # "cap2m": (97, 44),
            # "vhvi": (74, 21),
            # "uhvi": (74, 22),
            # "npn": (82, 20),
            # "inductor": (82, 24),
            # "vpp": (82, 64),
            # "pnp": (82, 44),
            # "lvs_prune": (84, 44),
            # "ncm": (92, 44),
            # "padcenter": (81, 20),
            # "mf": (76, 44),
            # "areaid_sl": (81, 1),
            # "areaid_ce": (81, 2),
            # "areaid_fe": (81, 3),
            # "areaid_sc": (81, 4),
            # "areaid_sf": (81, 6),
            # "areaid_sw": (81, 7),
            # "areaid_sr": (81, 8),
            # "areaid_mt": (81, 10),
            # "areaid_dt": (81, 11),
            # "areaid_ft": (81, 12),
            # "areaid_ww": (81, 13),
            # "areaid_ld": (81, 14),
            # "areaid_ns": (81, 15),
            # "areaid_ij": (81, 17),
            # "areaid_zr": (81, 18),
            # "areaid_ed": (81, 19),
            # "areaid_de": (81, 23),
            # "areaid_rd": (81, 24),
            # "areaid_dn": (81, 50),
            # "areaid_cr": (81, 51),
            # "areaid_cd": (81, 52),
            # "areaid_st": (81, 53),
            # "areaid_op": (81, 54),
            # "areaid_en": (81, 57),
            # "areaid_en20": (81, 58),
            # "areaid_le": (81, 60),
            # "areaid_hl": (81, 63),
            # "areaid_sd": (81, 70),
            # "areaid_po": (81, 81),
            # "areaid_it": (81, 84),
            # "areaid_et": (81, 101),
            # "areaid_lvt": (81, 108),
            # "areaid_re": (81, 125),
            # "areaid_ag": (81, 79),
            # "poly_rs": (66, 13),
            # "diff_rs": (65, 13),
            # "pwell_rs": (64, 13),
            # "li_rs": (67, 13),
            # "cfom": (22, 20),
        }

        # pin_prims = {
        #     name: prm.Marker(f"{name}.pin", gds_layer=(prim_layers[name], 1))
        #     for name in ("active", "poly", *(f"metal{n + 1}" for n in range(10)))
        # }
        # prims += pin_prims.values()

        # block_prims = {
        #     name: prm.Marker(f"{name}.block", gds_layer=(prim_layers[name], 2))
        #     for name in (
        #         "active", "poly", "contact",
        #         # *(f"via{n + 1}" for n in range(9)),
        #         *(f"metal{n + 1}" for n in range(10)),
        #     )
        # }
        # prims += block_prims.values()

        # single mask based primitives
        prims += (
            # implants
            *(
                prm.Implant(implant,
                    gds_layer=prim_layers[implant], type_=type_,
                    min_width=0.38, # n/ psd.1
                    min_space=0.38, # n/ psd.2
                    # n/ psd.10a+b, lvtn.13, hvtp.5
                    # Website says 0.255 for psdm but klayout DRC says 0.265
                    min_area=0.265,
                    # TODO: implement min_hole_area
                    # min_hole_area=0.265 # n/ psd.11, lvtn.14, hvtp.6
                ) for implant, type_ in (
                    ("nsdm", "n"),
                    ("psdm", "p"),
                    ("lvtn", "adjust"),
                    ("hvtp", "adjust"),
                )
            ),
            # wells
            prm.Well("nwm",
                gds_layer=prim_layers["nwm"], type_="n",
                min_width=0.840, # nwell.1
                min_space=1.270, # nwell.2a
            ),
            # depositions
            prm.Insulator("hvi",
                gds_layer=prim_layers["hvi"], fill_space="yes",
                min_width = 0.600, min_space = 0.700,
            ),
            # TODO: Resistor layers
            # prm.ExtraProcess(name="sblock",
            #     gds_layer=prim_layers["sblock"], fill_space="yes",
            #     min_width=0.045, # Own rule
            #     min_space=0.045, # Own rule
            # ),
        )
        # TODO: dnm
        # prims += prm.Well("dnm",
        #     gds_layer=prim_layers["dnm"], type_="n",
        #     min_width=3.000, # dnwell.2
        #     min_space=6.300, # dnwell.3
        #     deep_connect=prims.nwm,
        #     deep_connect_enclosing_hole=1.030, #nwell.6
        #     deep_connect_enclosed=0.400, # nwell.5
        # )

        prims += (
            # layers diff and tap will be generated out of difftap
            prm.WaferWire("difftap", gds_layer=prim_layers["difftap"],
                # pin=pin_prims["difftap"], blockage=block_prims["difftap"],
                min_width=0.150, # difftap.1
                min_space=0.270, # difftap.3
                allow_in_substrate=True, well=prims.nwm,
                implant=(prims.nsdm, prims.psdm),
                min_implant_enclosure=prp.Enclosure(0.125), # n/ psd.5a+b
                implant_abut="all", # n/ psd.6
                allow_contactless_implant=False,
                min_well_enclosure=prp.Enclosure(0.180), # difftap.8+10
                min_substrate_enclosure=prp.Enclosure(0.340), # difftap.9
                # TODO: implement min_(substrate/well)_enclosure_same_type
                # min_substrate_enclosure_same_type=prp.Enclosure(0.130), # difftap.11
                allow_well_crossing=False,
                oxide=prims.hvi,
            ),
            prm.GateWire("poly", gds_layer=prim_layers["poly"],
                # pin=pin_prims["poly"], blockage=block_prims["poly"],
                min_width=0.150, # poly.1a
                min_space=0.210, # poly.2
            ),
        )
        # wires
        prims += (
            *(prm.MetalWire(name, **wire_args) for name, wire_args in (
                ("li", {
                    "gds_layer": prim_layers["li"],
                    # "pin": pin_prims["li"],
                    # "blockage": block_prims["li"],
                    "min_width": 0.170, # li.1.-
                    "min_space": 0.170, # li.3.-
                }),
                *(
                    (metal, {
                        "gds_layer": prim_layers[metal],
                        # "pin": pin_prims[metal],
                        # "blockage": block_prims[metal],
                        "min_width": 0.140, # m1.1, m2.1
                        # TODO: implement max_width
                        # "max_width": 4.000, # m1.11, m2.11
                        "min_space": 0.140, # m1.2, m2.2
                        "space_table": ((1.5, 0.280),), # m1.3a+b, m2.3a+b
                        "min_area": 0.0676, # m1.6, m2.6
                        # TODO: implement min_hole_area
                        # "min_hole_area": 0.14, # m1.7, m2.7
                    }) for metal in ('m1', "m2")
                ),
                *(
                    (metal, {
                        "gds_layer": prim_layers[metal],
                        # "pin": pin_prims[metal],
                        # "blockage": block_prims[metal],
                        "min_width": 0.300, # m3.1, m4.1
                        # TODO: implement max_width
                        # m3.11, m4.11
                        # "max_width": 4.000 if metal == "m3" else 11.000,
                        "min_space": 0.300, # m3.2, m4.2
                        "space_table": ((1.5, 0.400),), # m3.3c+d, m4.3a+b
                        "min_area": 0.240, # m3.7, m4.7
                        # TODO: implement min_hole_area
                        # "min_hole_area": 0.200, # m3.7, m3.7
                    }) for metal in ("m3", "m4")
                ),
                ("m5", {
                    "gds_layer": prim_layers["m5"],
                    # "pin": pin_prims["metal9"],
                    # "blockage": block_prims["metal9"],
                    "min_width": 1.600, # m5.1
                    "min_space": 1.600, # m5.2
                    "min_area": 4.000, # m5.4
                }),
            )),
        )
        # TODO: RDL option

        # vias
        prims += (
            *(
                prm.Via(**via_args) for via_args in (
                    {
                        "name": "licon",
                        "gds_layer": prim_layers["licon"],
                        # "blockage": block_prims["licon"],
                        "width": 0.170, # licon.1
                        "min_space": 0.170, # licon.2
                        "bottom": (prims.difftap, prims.poly), # licon.4
                        "top": prims.li, # licon.4
                        "min_bottom_enclosure": (
                            prp.Enclosure((0.040, 0.060)), # licon.5a+c
                            prp.Enclosure((0.050, 0.080)), # licon.8+a
                        ),
                        "min_top_enclosure": prp.Enclosure((0.000, 0.080)), # li.5.-
                    },
                    {
                        "name": "mcon",
                        "gds_layer": prim_layers["mcon"],
                        # "blockage": block_prims["mcon"],
                        "width": 0.170, # ct.1
                        "min_space": 0.190, # ct.2
                        "bottom": prims.li,
                        "top": prims.m1,
                        "min_bottom_enclosure": prp.Enclosure(0.000), # ct.4
                        "min_top_enclosure": prp.Enclosure((0.030, 0.060)), # m1.4+5
                    },
                    {
                        "name": "via",
                        "gds_layer": prim_layers["via"],
                        # "blockage": block_prims["via"],
                        "width": 0.150, # via.1a
                        "min_space": 0.170, # via.2
                        "bottom": prims.m1,
                        "top": prims.m2,
                        "min_bottom_enclosure": prp.Enclosure((0.055, 0.085)), # via.4a+5a
                        "min_top_enclosure": prp.Enclosure((0.055, 0.085)), # m2.4+5
                    },
                    {
                        "name": "via2",
                        "gds_layer": prim_layers["via2"],
                        # "blockage": block_prims["via2"],
                        "width": 0.200, # via2.1a
                        "min_space": 0.200, # via2.2
                        "bottom": prims.m2,
                        "top": prims.m3,
                        "min_bottom_enclosure": prp.Enclosure((0.040, 0.085)), # via2.4+5
                        "min_top_enclosure": prp.Enclosure(0.065), # m3.4
                    },
                    {
                        "name": "via3",
                        "gds_layer": prim_layers["via3"],
                        # "blockage": block_prims["via3"],
                        "width": 0.200, # via3.1
                        "min_space": 0.200, # via3.2
                        "bottom": prims.m3,
                        "top": prims.m4,
                        "min_bottom_enclosure": prp.Enclosure((0.060, 0.090)), # via3.4+5
                        "min_top_enclosure": prp.Enclosure(0.065), # m4.3
                    },
                    {
                        "name": "via4",
                        "gds_layer": prim_layers["via4"],
                        # "blockage": block_prims["via4"],
                        "width": 0.800, # via4.1
                        "min_space": 0.800, # via4.2
                        "bottom": prims.m4,
                        "top": prims.m5,
                        "min_bottom_enclosure": prp.Enclosure(0.190), # via4.4
                        "min_top_enclosure": prp.Enclosure(0.310), # m5.3
                    },
                )
            ),
            prm.PadOpening("pad", gds_layer=prim_layers["pad"],
                # TODO: Can min_width be reduced ?
                min_width=40.000, # Own rule
                min_space=1.270, # pad.2
                bottom=prims.m5,
                min_bottom_enclosure=prp.Enclosure(1.000), # Own rule
            )
        )

        # misc using wires
        # prims += (
        #     # resistors
        #     *(
        #         prm.Resistor(name,
        #             wire=wire, indicator=prims.sblock,
        #             min_indicator_extension=0.045, # Own rule
        #             sheetres=sheetres, # Own rulw
        #         )
        #         for name, wire, sheetres in (
        #             ("active_res", prims.difftap, 200.0),
        #             ("poly_res", prims.poly, 300.0),
        #         )
        #     ),
        #     # extra space rules
        #     prm.Spacing(primitives1=prims.difftap, primitives2=prims.poly, min_space=0.050), # Poly.5
        #     prm.Spacing(primitives1=prims.licon, primitives2=prims.poly, min_space=0.090),
        # )

        # transistors
        prims += (
            prm.MOSFETGate("mosgate",
                active=prims.difftap, poly=prims.poly,
                # No need for overruling min_l, min_w
                min_sd_width=0.070, # Poly.4
                min_polyactive_extension=0.055, # Poly.3
                contact=prims.licon, min_contactgate_space=0.035, # Contact.6
                min_gate_space=0.140, #
            ),
            prm.MOSFETGate("hvmosgate",
                active=prims.difftap, poly=prims.poly, oxide=prims.hvi,
                min_l=0.060, # Added rule
                min_sd_width=0.070, # Poly.4
                min_polyactive_extension=0.055, # Poly.3
                contact=prims.licon, min_contactgate_space=0.035, # Contact.6
                min_gate_space=0.140, #
            ),
        )
        prims += (
            prm.MOSFET(name, model=model,
                gate=gate, implant=impl, well=well,
                min_gateimplant_enclosure=prp.Enclosure(0.070), # Implant.1
            ) for name, model, gate, impl, well in (
                (
                    "nfet_01v8", "sky130_fd_pr__nfet_01v8__model",
                    prims.mosgate, prims.nsdm, None,
                ),
                (
                    "pfet_01v8", "sky130_fd_pr__pfet_01v8__model",
                    prims.mosgate, prims.psdm, prims.nwm,
                ),
                (
                    "nfet_01v8_lvt", "sky130_fd_pr__nfet_01v8_lvt__model",
                    prims.mosgate, (prims.nsdm, prims.lvtn), None,
                ),
                (
                    "pfet_01v8_lvt", "sky130_fd_pr__pfet_01v8_lvt__model",
                    prims.mosgate, (prims.psdm, prims.lvtn), prims.nwm,
                ),
                (
                    "pfet_01v8_hvt", "sky130_fd_pr__pfet_01v8_hvt__model",
                    prims.mosgate, (prims.psdm, prims.hvtp), prims.nwm,
                ),
                (
                    "nfet_g5v0d10v5", "sky130_fd_pr__nfet_g5v0d10v5__model",
                    prims.hvmosgate, prims.nsdm, None,
                ),
                (
                    "pfet_g5v0d10v5", "sky130_fd_pr__pfet_g5v0d10v5__model",
                    prims.hvmosgate, prims.psdm, prims.nwm,
                ),
            )
        )

tech = technology = _Sky130()
cktfab = circuit_factory = ckt.CircuitFactory(tech)
layoutfab = layout_factory = lay.LayoutFactory(tech)
plotter = lay.Plotter({
    # "pwell": {"fc": (1.0, 1.0, 0.0, 0.2), "ec": "orange", "zorder": 10},
    "nwm": {"fc": (0.0, 0.0, 0.0, 0.1), "ec": "grey", "zorder": 10},
    "difftap": {"fc": "lawngreen", "ec": "lawngreen", "zorder": 11},
    "poly": {"fc": "red", "ec": "red", "zorder": 12},
    "nsdm": {"fc": "purple", "ec": "purple", "alpha": 0.3, "zorder": 13},
    "psdm": {"fc": "blueviolet", "ec": "blueviolet", "alpha": 0.3, "zorder": 13},
    "lvtn": {"fc": (0.0, 0.0, 0.0, 0.0), "ec": "grey", "zorder": 13},
    "hvtp": {"fc": (1, 1, 1, 0.3), "ec": "whitesmoke", "zorder": 13},
    "licon": {"fc": "black", "ec": "black", "zorder": 14},
    "li": {"fc": (0.1, 0.1, 1, 0.4), "ec": "blue", "zorder": 15},
})
