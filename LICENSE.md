The code in this repository and the generated files from it are multi-licensed. It can be distributed under any of the following licenses:

1. [GNU General Public License V2.0](LICENSES/gpl-2.0.txt) or any later version (GPL-2.0-or-later)
2. [GNU Aferro General Public License V3.0](LICENSES/agpl-3.0.txt) or any later version (A)
3. [CERN Open Hardware Licence Version 2 - Strongly Reciprocal](LICENSES/cern_ohl_s_v2.txt) (CERN-OHL-S-2.0) or any later version of the license.

In the future, the license list may be reduced.
